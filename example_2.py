import os
from pathlib import Path

entries = os.listdir("files")
print(entries)


entries_modern = os.scandir("files")
print(list(entries_modern))


entries_pathlib = Path("files")
for enrty in entries_pathlib.iterdir():
    print(enrty.name)

print("-" * 100)
base_path = "files"

for entry in os.listdir(base_path):
    if os.path.isfile(os.path.join(base_path, entry)):
        print(entry)

print("-" * 100)

with os.scandir(base_path) as entries:
    for entry in entries:
        if entry.is_file():
            print(entry.name)


print("-" * 100)

path = Path(base_path)
for item in path.iterdir():
    if item.is_file():
        print(item.name)

print("-" * 100)
with os.scandir(base_path) as dir_contents:
    for entry in dir_contents:
        info = entry.stat()
        print(entry.name, info.st_mtime)


print("-" * 100)

path = Path(base_path)
for item in path.iterdir():
    info = item.stat()
    print(info.st_mtime)


# os.mkdir("files_os")

try:
    path = Path("files_pathlib")
    path.mkdir()
except FileExistsError as err:
    print(err)

# os.makedirs("files_os/2023/10", mode=0o770)


path = Path("files_pathlib/2023/10")
path.mkdir(parents=True)
